package com.akvelon.carfleet.model.car

import com.akvelon.carfleet.model.engine.Engine
import com.akvelon.carfleet.model.person.Driver
import java.io.Serializable

open class Car(
    val brand: String,
    val transmissionType: Transmission,
    val weight: Float,
    val driver: Driver,
    val engine: Engine,
    val imageUrl: String,
    val iconUrl: String
) : Serializable {
    override fun toString() =
        "Brand: $brand\nTransmission: $transmissionType\nWeight: $weight\nDriver: $driver\nEngine: $engine"
}
