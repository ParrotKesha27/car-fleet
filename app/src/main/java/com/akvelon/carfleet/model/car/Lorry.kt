package com.akvelon.carfleet.model.car

import com.akvelon.carfleet.model.engine.Engine
import com.akvelon.carfleet.model.person.Driver

class Lorry(
    brand: String,
    transmissionType: Transmission,
    weight: Float,
    driver: Driver,
    engine: Engine,
    imageUrl: String,
    iconUrl: String,
    val liftingCapacity: Float
) : Car(
    brand,
    transmissionType,
    weight,
    driver,
    engine,
    imageUrl,
    iconUrl
) {
    override fun toString() = "${super.toString()}\nLifting capacity: $liftingCapacity"
}
