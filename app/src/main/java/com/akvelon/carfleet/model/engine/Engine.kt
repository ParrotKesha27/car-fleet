package com.akvelon.carfleet.model.engine

import java.io.Serializable

class Engine(
    val power: Int,
    val vendor: String
) : Serializable {
    override fun toString() = "Power: $power; Vendor $vendor"
}
