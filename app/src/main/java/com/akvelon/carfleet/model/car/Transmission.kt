package com.akvelon.carfleet.model.car

enum class Transmission {
    AUTOMATIC,
    MANUAL
}
