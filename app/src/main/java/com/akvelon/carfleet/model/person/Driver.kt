package com.akvelon.carfleet.model.person

import com.akvelon.carfleet.util.toDetailString
import java.time.LocalDate
import java.time.Period

class Driver(
    fullName: String,
    photoUrl: String,
    val drivingDateStart: LocalDate
) : Person(
    fullName,
    photoUrl
) {
    val experience: Period
        get() = Period.between(drivingDateStart, LocalDate.now())

    override fun toString() = "${super.toString()}; Experience: ${experience.toDetailString()}"
}
