package com.akvelon.carfleet.model.person

import java.io.Serializable

open class Person(
    val fullName: String,
    val photoUrl: String
) : Serializable {
    override fun toString() = "Full name: $fullName"
}
