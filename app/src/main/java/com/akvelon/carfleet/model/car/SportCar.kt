package com.akvelon.carfleet.model.car

import com.akvelon.carfleet.model.engine.Engine
import com.akvelon.carfleet.model.person.Driver

class SportCar(
    brand: String,
    transmissionType: Transmission,
    weight: Float,
    driver: Driver,
    engine: Engine,
    imageUrl: String,
    iconUrl: String,
    val maxSpeed: Int
) : Car(
    brand,
    transmissionType,
    weight,
    driver,
    engine,
    imageUrl,
    iconUrl
) {
    override fun toString() = "${super.toString()}\nMax speed: $maxSpeed"
}
