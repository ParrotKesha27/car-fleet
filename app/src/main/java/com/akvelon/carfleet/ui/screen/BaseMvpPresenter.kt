package com.akvelon.carfleet.ui.screen

import com.akvelon.carfleet.App
import io.reactivex.disposables.CompositeDisposable
import moxy.MvpPresenter
import moxy.MvpView

open class BaseMvpPresenter<V : MvpView> : MvpPresenter<V>() {
    protected val router = App.instance.router

    protected val disposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()

        disposable.clear()
    }
}
