package com.akvelon.carfleet.ui.screen.car.details

import com.akvelon.carfleet.ui.screen.BaseMvpPresenter
import moxy.InjectViewState

@InjectViewState
class CarDetailsPresenter : BaseMvpPresenter<CarDetailsView>() {
    override fun onFirstViewAttach() {
        viewState.showCar()
    }
}