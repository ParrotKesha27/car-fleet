package com.akvelon.carfleet.ui.screen.car.list

import android.util.Log
import com.akvelon.carfleet.model.car.Car
import com.akvelon.carfleet.repository.CarRepository
import com.akvelon.carfleet.ui.screen.BaseMvpPresenter
import com.akvelon.carfleet.ui.screen.Screens
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState

@InjectViewState
class CarListPresenter : BaseMvpPresenter<CarListView>() {
    override fun onFirstViewAttach() {
        getAllCars()
    }

    fun onCarClick(car: Car) = router.navigateTo(Screens.CarDetails(car))

    private fun getAllCars() = CarRepository.getAllCars()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(
            onSuccess = {
                viewState.showCars(it)
            },
            onError = {
                Log.e("CarList", it.toString())
            }
        )
        .addTo(disposable)
}
