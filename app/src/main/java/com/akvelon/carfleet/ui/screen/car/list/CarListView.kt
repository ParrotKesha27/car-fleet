package com.akvelon.carfleet.ui.screen.car.list

import com.akvelon.carfleet.model.car.Car
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

interface CarListView : MvpView {
    @AddToEndSingle
    fun showCars(cars: List<Car>)
}
