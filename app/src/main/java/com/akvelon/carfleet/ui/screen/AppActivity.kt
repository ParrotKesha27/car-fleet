package com.akvelon.carfleet.ui.screen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.akvelon.carfleet.App
import com.akvelon.carfleet.R
import com.github.terrakok.cicerone.androidx.AppNavigator

class AppActivity : AppCompatActivity() {
    private val navigator = AppNavigator(this, R.id.appContainer)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            App.instance.router.newRootScreen(Screens.CarList())
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        App.instance.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        App.instance.navigatorHolder.removeNavigator()
        super.onPause()
    }
}
