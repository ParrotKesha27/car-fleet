package com.akvelon.carfleet.ui.screen.car.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akvelon.carfleet.R
import com.akvelon.carfleet.model.car.Car
import com.akvelon.carfleet.util.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_car.view.*

class CarListAdapter(
    private val cars: List<Car>,
    private val carClickListener: (Car) -> Unit
) : RecyclerView.Adapter<CarListAdapter.CarHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CarHolder(
            parent.inflate(R.layout.item_car)
        )

    override fun onBindViewHolder(holder: CarHolder, position: Int) =
        holder.bind(cars[position], carClickListener)

    override fun getItemCount() = cars.size

    inner class CarHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(car: Car, clickListener: (Car) -> Unit) {
            view.apply {
                carTitle.text = car.brand
                carDriver.text = car.driver.fullName
                Picasso.get().load(car.iconUrl).into(carIcon)

                setOnClickListener {
                    clickListener(car)
                }
            }
        }
    }
}
