package com.akvelon.carfleet.ui.screen.car.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.akvelon.carfleet.R
import com.akvelon.carfleet.model.car.Car
import com.akvelon.carfleet.model.car.Lorry
import com.akvelon.carfleet.model.car.SportCar
import com.akvelon.carfleet.util.toDetailString
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_characteristic.view.*
import kotlinx.android.synthetic.main.fragment_car_details.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class CarDetailsFragment :
    MvpAppCompatFragment(),
    CarDetailsView {

    private lateinit var car: Car

    @InjectPresenter
    lateinit var presenter: CarDetailsPresenter

    companion object {
        const val CAR_FIELD = "car"

        fun newInstance(car: Car) =
            CarDetailsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(CAR_FIELD, car)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            car = it.getSerializable(CAR_FIELD) as Car
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_car_details, container, false)
    }

    override fun showCar() {
        showBaseCar()

        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        val additionalCard = layoutInflater.inflate(
            R.layout.card_characteristic,
            characteristicsContainer,
            false
        )

        when (car) {
            is SportCar -> showSportCarCharacteristics(additionalCard)
            is Lorry -> showLorryCharacteristics(additionalCard)
        }
    }

    private fun showBaseCar() = with(car) {
        Picasso.get().load(imageUrl).into(carImage)

        carBrand.text = brand
        engineVendor.text = engine.vendor
        enginePowerText.text = getString(R.string.engine_power).format(engine.power)
        transmissionText.text = transmissionType.toString()
        weightText.text = getString(R.string.weight).format(weight)

        with(driver) {
            driverName.text = fullName
            driverExperience.text = experience.toDetailString()
            Picasso.get().load(photoUrl).into(driverImage)
        }
    }

    private fun showSportCarCharacteristics(card: View) {
        card.apply {
            characteristicImage.setImageResource(R.drawable.ic_max_speed)
            characteristicText.text =
                this.context.getString(R.string.max_speed).format((car as SportCar).maxSpeed)
        }
        characteristicsContainer.addView(card, 0)
    }

    private fun showLorryCharacteristics(card: View) {
        card.apply {
            characteristicImage.setImageResource(R.drawable.ic_lifting_capacity)
            characteristicText.text = this.context.getString(R.string.lifting_capacity)
                .format((car as Lorry).liftingCapacity)
        }
        characteristicsContainer.addView(card, 0)
    }
}
