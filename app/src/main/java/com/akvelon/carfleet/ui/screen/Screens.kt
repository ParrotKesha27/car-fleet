package com.akvelon.carfleet.ui.screen

import com.akvelon.carfleet.model.car.Car
import com.akvelon.carfleet.ui.screen.car.details.CarDetailsFragment
import com.akvelon.carfleet.ui.screen.car.list.CarListFragment
import com.github.terrakok.cicerone.androidx.FragmentScreen

object Screens {
    fun CarList() = FragmentScreen { CarListFragment.newInstance() }
    fun CarDetails(car: Car) = FragmentScreen { CarDetailsFragment.newInstance(car) }
}
