package com.akvelon.carfleet.ui.screen.car.details

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle

interface CarDetailsView : MvpView {
    @AddToEndSingle
    fun showCar()
}
