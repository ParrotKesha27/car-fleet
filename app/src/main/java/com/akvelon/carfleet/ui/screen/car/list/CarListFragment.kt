package com.akvelon.carfleet.ui.screen.car.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.akvelon.carfleet.R
import com.akvelon.carfleet.model.car.Car
import kotlinx.android.synthetic.main.fragment_car_list.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class CarListFragment :
    MvpAppCompatFragment(),
    CarListView {

    @InjectPresenter
    lateinit var presenter: CarListPresenter

    companion object {
        fun newInstance() = CarListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_car_list, container, false)
    }

    override fun showCars(cars: List<Car>) {
        carsRecyclerView.adapter = CarListAdapter(cars, presenter::onCarClick)
    }
}
