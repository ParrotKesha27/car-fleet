package com.akvelon.carfleet.api.car

import com.akvelon.carfleet.model.car.Car
import com.akvelon.carfleet.model.car.Lorry
import com.akvelon.carfleet.model.car.SportCar

data class CarsResponse(
    val cars: List<Car>,
    val sportCars: List<SportCar>,
    val lorries: List<Lorry>
) {
    fun toList() = cars + sportCars + lorries
}
