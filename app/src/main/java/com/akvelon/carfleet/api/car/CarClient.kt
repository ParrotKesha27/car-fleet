package com.akvelon.carfleet.api.car

import io.reactivex.Single
import retrofit2.http.GET

interface CarClient {
    @GET("/v3/ebdda590-b3e0-4d07-87b7-ba6a8dd2d5da")
    fun getAllCars(): Single<CarsResponse>
}
