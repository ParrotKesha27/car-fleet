package com.akvelon.carfleet.repository

import com.akvelon.carfleet.api.car.CarClient
import com.akvelon.carfleet.api.HttpClient
import com.akvelon.carfleet.api.car.CarsResponse

object CarRepository {
    private val carHttpClient = HttpClient.buildService(CarClient::class.java)

    fun getAllCars() = carHttpClient.getAllCars()
        .map(CarsResponse::toList)
}
